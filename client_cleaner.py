#! /usr/bin/env python3

from datetime import (datetime, timedelta)
import ftplib
import getpass
import os
import csv

import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import numpy as np
import pandas as pd
import psycopg2

def get_or_prompt(var, private=True):
    if var in os.environ:
        return os.environ[var]
    elif not private:
        input('{var}: '.format(var=var))
    else:
        return getpass.getpass(prompt='{var}: '.format(var=var))

def load_database():
    password = get_or_prompt("DB_PASSWORD")
    print("DB CONNECTION ... ", end='')
    conn = psycopg2.connect(
        host="souscritootest.cuarmbocgsq7.eu-central-1.rds.amazonaws.com",
        port="5432",
        user="testread",
        password=password,
        dbname="souscritootest"
    )
    cur = conn.cursor()
    print("OK")
    yesterday = datetime.now() - timedelta(days=1)
    date = yesterday.strftime("%Y-%m-%d")
    # date = "2016-01-15" -> use to debug
    cur.execute("SELECT * FROM clients_crm WHERE \"CreationDate\" = '{date}'".format(date=date))
    columns = ['id', 'firstname', 'lastname', 'phone', 'created_at']
    crm_clients = pd.DataFrame(data=cur.fetchall(), columns=columns)
    cur.close()
    conn.close()
    return crm_clients


def load_ftp():
    password = get_or_prompt("FTP_PASSWORD")
    print("FTP CONNECTION ... ", end='')
    ftp = ftplib.FTP(host="35.157.119.136")
    ftp.login(user="candidate", passwd=password)
    print("OK")
    calls = []
    # Read and split CSV file in memory
    ftp.retrlines(
        'RETR files/raw_calls.csv',
        lambda l: calls.append(l.split(','))
    )
    calls = pd.DataFrame(columns=calls[0], data=calls[1:])
    return calls


def transform(crm_clients, calls):
    print("TRANSFORM DATA ... ", end='')
    # Change column name
    calls.columns = ["id", "called", "date", "duration", "phone"]
    # Drop hidden phone numbers
    calls = calls.replace('', np.nan).dropna()
    # Change column type to apply mean
    calls.duration =  calls.duration.astype('int64')
    # Create GroupBy DataFrame
    calls_gb = calls[["date", "duration", "phone"]].groupby(by=["phone"])
    # Aggregate date using min() and duration using mean()
    calls = calls_gb.agg({"date": np.min, "duration": np.mean})
    calls = calls.reset_index()
    calls.phone = "0" + calls.phone
    crm_clients = crm_clients[["id", "firstname", "lastname", "phone"]]
    clients = pd.merge(crm_clients, calls, on="phone", how="left")
    clients.columns = ["ID", "FirstName", "LastName", "PhoneNumber", "FirstCallDate", "Duration"]
    print("OK")
    return clients

def send_email(crm_clients, clients):
    print("CREATE DATA FILE ... ", end='')
    filename = "clients.csv"
    clients.to_csv(filename, index=False)
    print("OK")
    # Collecting informations
    host = get_or_prompt("EMAIL_HOST", private=False)
    port = get_or_prompt("EMAIL_PORT", private=False)
    address = get_or_prompt("EMAIL_ADDRESS", private=False)
    password = get_or_prompt("EMAIL_PASSWORD")
    print("LOGIN EMAIL SERVER ... ", end='')
    context = ssl.create_default_context()
    server = smtplib.SMTP_SSL(host, port, context=context)
    server.ehlo()
    server.login(address, password)
    print("OK")
    print("SENDING EMAIL ... ", end='')
    # General config
    msg = MIMEMultipart()
    msg['Subject'] = 'Clients daily report'
    msg['From'] = address
    msg['To'] = address
    # Body creation
    body = crm_clients[["firstname", "lastname"]].to_csv(
        index=False,
        line_terminator="<br/>",
        quoting=csv.QUOTE_NONNUMERIC,
    )
    msg.attach(MIMEText(body, 'html'))
    # File attachment
    with open(filename, 'r') as fd:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(fd.read())
    encoders.encode_base64(part)
    part.add_header("Content-Disposition", f"attachment; filename= {filename}")
    msg.attach(part)
    # Sending
    server.sendmail(address, address, msg.as_string())
    print("OK")


def main():
    crm_clients = load_database()
    calls = load_ftp()
    clients = transform(crm_clients, calls)
    send_email(crm_clients, clients)


if __name__ == "__main__":
    main()